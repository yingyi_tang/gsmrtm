import commands
import datetime
import globVar
import logging
import logging.config
import os
import shutil
import json
import ConfigParser

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("alarmlog")
except Exception, e:
    print(e)

MANAGED_BSC_NAME_FILE = os.path.join(globVar.INPUT_DIR, "managed_bsc_name.json")

DELTA = -5

def genAlarm():
    now = datetime.datetime.now()
    cur_date = now.strftime("%m/%d/%Y")
    end_time = now.strftime("%H:%M") + ":00"
    delta = datetime.timedelta(minutes=DELTA)
    start_time = (now + delta).strftime("%H:%M") + ":00"
    logger.info("alarm log search time from {t1} to {t2}".format(date=cur_date, t1=start_time, t2=end_time))

    # generate all bsc oor for filter file
    with open(os.path.join(globVar.INPUT_DIR, "managed_bsc_oor.json"), "r") as f:
        oorsList = json.load(f)
    oorsStr = "\n".join(oorsList)

    filterFile = os.path.join(globVar.TEMP_DIR, "filter.txt")
    shutil.copy(globVar.TEMPLATE_FILE, filterFile)
    logger.info("copied {sfile} to {dfile}".format(sfile=globVar.TEMPLATE_FILE, dfile=filterFile))
    with open(filterFile, "r") as f:
        filterContent = f.readlines()

    filterContent[1] = oorsStr + "\n"
    filterContent[3] = filterContent[5] = cur_date + "\n"
    filterContent[4] = start_time + "\n"
    filterContent[6] = end_time + "\n"
    with open(filterFile, "w") as f:
        f.writelines(filterContent)
    logger.info("set oor and time in {file}".format(file=filterFile))

    _, domainname = commands.getstatusoutput('domainname')
    fileName = domainname + "_alarmlog_" + now.strftime("%Y%m%d%H%M") + "00"
    alarmLogFile = os.path.join(globVar.OUTPUT_DIR, fileName)
    #cmd = "/opt/ericsson/bin/als_search -read {filterfile} -mib {dn} -olf {outfile}".format(filterfile=filterFile, dn=domainname, outfile=alarmLogFile)
    cmd = "/opt/ericsson/bin/als_search -read {filterfile} -mib {dn} -exp {outfile}".format(filterfile=filterFile, dn=domainname, outfile=alarmLogFile)
    logger.info("ready to run {cmd}".format(cmd=cmd))
    status, ret = commands.getstatusoutput(cmd)
    if status == 0:
        logger.info("generated {file}".format(file=alarmLogFile))
    else:
         logger.error(ret)

    # get external link dir
    conf = ConfigParser.ConfigParser()
    conf.read(globVar.CONIF_FILE)
    link_dir = conf.get("external", "link_directory")
    logger.info("get symbolic link directory {dir}".format(dir=link_dir))

    if os.path.isdir(link_dir):
        linkFile = os.path.join(link_dir, fileName)
        os.symlink(alarmLogFile, linkFile)
        logger.info("link {dstFile} to {srcFile}".format(dstFile=linkFile, srcFile=alarmLogFile))
    else:
        logger.error("link directory {dir} is not exist".format(dir=link_dir))

def houseKeeping():
    conf = ConfigParser.ConfigParser()
    conf.read(globVar.CONIF_FILE)
    link_dir = conf.get("external", "link_directory")
    logger.info("get symbolic link directory {dir}".format(dir=link_dir))

    try:
        cmd = "/bin/find {outputDir}/ -mtime +2 -type f -exec rm {char} \;".format(outputDir=globVar.OUTPUT_DIR, char="{}")
        status, ret = commands.getstatusoutput(cmd)
        if status==0:
            logger.info("run {cmd}".format(cmd=cmd))
        else:
            logger.info("run {cmd} failed".format(cmd=cmd))

        cmd = "/bin/find {link_dir}/ -mtime +2 -type f -exec rm {char} \;".format(link_dir=link_dir, char="{}")
        status, ret = commands.getstatusoutput(cmd)
        if status==0:
            logger.info("run {cmd}".format(cmd=cmd))
        else:
            logger.info("run {cmd} failed".format(cmd=cmd))
        # M
        fsize = os.path.getsize(globVar.LOG_FILE)/1024/2014
        if fsize >= 10:
            os.remove(globVar.LOG_FILE)
            logger.info("log file size {size}, remove {file}".format(size=fsize, file=globVar.LOG_FILE))
    except Exception, e:
        logger.error(e)

if __name__ == "__main__":
    genAlarm()
    houseKeeping()
