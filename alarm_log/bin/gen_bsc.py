import globVar
import os
import commands
import logging
import logging.config
import json

# /opt/ericsson/ddc/util/bin/listnodes | grep -i "BSC TRC"

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("alarmlog")
except Exception, e:
    print(e)

TMP_FILE = os.path.join(globVar.TEMP_DIR, "gen_bsc_listnode.tmp")
MANAGED_BSC_NAME_FILE = os.path.join(globVar.INPUT_DIR, "managed_bsc_name.json")
MANAGED_BSC_OOR_FILE = os.path.join(globVar.INPUT_DIR, "managed_bsc_oor.json")

def genBsc():
    cmd = "/opt/ericsson/ddc/util/bin/listnodes | grep \"BSC TRC\" > {tmpFile}".format(tmpFile=TMP_FILE)
    status, ret = commands.getstatusoutput(cmd)
    logger.info("run {cmd}".format(cmd=cmd))
    if status == 0:
        logger.info("generated {file}".format(file=TMP_FILE))
    else:
        for line in  ret:
            logger.error(line)

    with open(TMP_FILE, "r") as f:
        lines = f.readlines()
    bscNameList = []
    bscOorList = []
    for line in lines:
        try:
            bscName = line.split("@")[0].split("=")[3]
            bscOor = line.split("@")[0]
        except Exception, e:
            logger.error(e)
        if bscName and bscName not in bscNameList:
            bscNameList.append(bscName)
        if bscOor and bscOor not in bscOorList:
            bscOorList.append(bscOor)
    logger.info("found total {num} bsc".format(num=len(bscNameList)))

    with open(MANAGED_BSC_NAME_FILE, "w") as f:
        json.dump(bscNameList, f)
    logger.info("generated {file}".format(file=MANAGED_BSC_NAME_FILE))
    with open(MANAGED_BSC_OOR_FILE, "w") as f:
        json.dump(bscOorList, f)

if __name__ == "__main__":
    genBsc()
