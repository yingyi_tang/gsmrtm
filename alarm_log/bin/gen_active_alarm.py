import commands
import datetime
import globVar
import logging
import logging.config
import os
import shutil
import json
import ConfigParser

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("alarmlog")
except Exception, e:
    print(e)

MANAGED_BSC_OOR_FILE = os.path.join(globVar.INPUT_DIR, "managed_bsc_oor.json")

def genActiveAlarm():
    now = datetime.datetime.now()
    cur_time = now.strftime("%Y%m%d%H%M") + "00"
    
    _, domainname = commands.getstatusoutput('domainname')
    filename = domainname + "_activeAlarmLog_" + cur_time
    conf = ConfigParser.ConfigParser()
    conf.read(globVar.CONIF_FILE)
    link_dir = conf.get("external", "link_directory")
    file = os.path.join(link_dir, filename)
    with open(MANAGED_BSC_OOR_FILE, "r") as f:
        bscOorList = json.load(f)
    for bscOor in bscOorList:
        cmd = "/opt/ericsson/bin/fmlist -oor {oor}".format(oor=bscOor)
        retCode, ret = commands.getstatusoutput(cmd)
        logger.info("run {cmd}".format(cmd=cmd))
        with open(file, "a") as f:
            f.write(ret)
    logger.info("generated {file}".format(file=file))

if __name__ == "__main__":
    genActiveAlarm()
