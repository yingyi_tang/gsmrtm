import commands
import datetime
import globVar
import logging
import logging.config
import os
import shutil
import json
import ConfigParser

try:
    logging.config.fileConfig(globVar.LOGGER_FILE)
    logger = logging.getLogger("alarmlog")
except Exception, e:
    print(e)

MANAGED_BSC_NAME_FILE = os.path.join(globVar.INPUT_DIR, "managed_bsc_name.json")

DELTA = -5

def genAlarm():
    now = datetime.datetime.now()
    cur_date = now.strftime("%m/%d/%Y")
    end_time = now.strftime("%H:%M") + ":00"
    delta = datetime.timedelta(minutes=DELTA)
    start_time = (now + delta).strftime("%H:%M") + ":00"
    logger.info("alarm log search time from {t1} to {t2}".format(date=cur_date, t1=start_time, t2=end_time))

    filterFile = os.path.join(globVar.TEMP_DIR, "filter.txt")
    if not os.path.isfile(filterFile):
        shutil.copy(globVar.TEMPLATE_FILE, filterFile)
        logger.info("copied {sfile} to {dfile}".format(sfile=globVar.TEMPLATE_FILE, dfile=filterFile))
    with open(filterFile, "r") as f:
        filterContent = f.readlines()

    filterContent[3] = filterContent[5] = cur_date + "\n"
    filterContent[4] = start_time + "\n"
    filterContent[6] = end_time + "\n"
    with open(filterFile, "w") as f:
        f.writelines(filterContent)
    logger.info("set time in {file}".format(file=filterFile))

    _, domainname = commands.getstatusoutput('domainname')
    fileName = domainname + "_alarmlog_" + now.strftime("%Y%m%d%H%M") + "00"
    alarmLogFile = os.path.join(globVar.OUTPUT_DIR, fileName)
    cmd = "/opt/ericsson/bin/als_search -read {filterfile} -mib {dn} -olf {outfile}".format(filterfile=filterFile, dn=domainname, outfile=alarmLogFile)
    logger.info("ready to run {cmd}".format(cmd=cmd))
    status, ret = commands.getstatusoutput(cmd)
    if status == 0:
        logger.info("generated {file}".format(file=alarmLogFile))
    else:
        for line in  ret:
            logger.error(line)

    # get external link dir
    conf = ConfigParser.ConfigParser()
    conf.read(globVar.CONIF_FILE)
    link_dir = conf.get("external", "link_directory")
    logger.info("get symbolic link directory {dir}".format(dir=link_dir))

    # generate alarm file for each bsc
    #alarmLogFile = "C:\\Users\\TANG\\Documents\\gsmrtm\\alarm_log\\output\\gzomc_alarmlog_20181226103000"
    with open(alarmLogFile, "r") as f:
        alarms = f.readlines()
    bscNameList = []
    with open(MANAGED_BSC_NAME_FILE, "r") as f:
        bscNameList = json.load(f)

    fileDate = now.strftime("%Y%m%d")
    fileStartTime = fileDate + (now + delta).strftime("%H%M") + "00"
    fileEndTime = fileDate + now.strftime("%H%M") + "00"
    dirTime = fileDate + now.strftime("%H")
    hourDelta = datetime.timedelta(hours=-1)
    lastDirTime = (now + hourDelta).strftime("%Y%m%d%H")
    #print(dirTime, lastDirTime)
    lastOutDir = os.path.join(globVar.OUTPUT_DIR, lastDirTime)
    outDir = os.path.join(globVar.OUTPUT_DIR, dirTime)

    # create a directory to store ROP file
    if not os.path.isdir(outDir):
        os.mkdir(outDir)
        logger.info("create directory {dirt}".format(dirt=outDir))
        if os.path.isdir(lastOutDir):
            zfile = globVar.OUTPUT_DIR + os.path.sep + domainname + "_alarmlog_" + lastDirTime + ".zip"
            cmd = "/usr/bin/zip -qjr {zfile} {zdir}".format(zfile=zfile, zdir=lastOutDir)
            status, ret = commands.getstatusoutput(cmd)
            if status == 0:
                logger.info("run {cmd}".format(cmd=cmd))
            else:
                for line in  ret:
                    logger.error(line)
            cmd = "/usr/bin/rm {linkDir}/*".format(linkDir=link_dir)
            status, ret = commands.getstatusoutput(cmd)
            if status == 0:
                logger.info("run {cmd}".format(cmd=cmd))
            else:
                for line in  ret:
                    logger.error(line)
            shutil.rmtree(lastOutDir)
            logger.info("remove directory {dir}".format(dir=lastOutDir))
        else:
            pass
    else:
        pass

    for bsc in bscNameList:
        bscAlarmList = []
        bscAlarmContent = []
        for alarm in alarms[1:]:
            oor = ""
            try:
                oor = alarm.split("@")[7]
            except Exception, e:
                logger.error(alarm)
                logger.error(e)
            if bsc in oor:
                bscAlarmList.append(alarm)
        bscAlarmContent = "\n".join(bscAlarmList)
        if bscAlarmContent:
            fileDate = now.strftime("%Y%m%d")
            fileStartTime = fileDate + (now + delta).strftime("%H%M") + "00"
            fileEndTime = fileDate + now.strftime("%H%M") + "00"
            fileName = bsc + "_" + fileStartTime + "_" + fileEndTime
            file = os.path.join(outDir, fileName)
            linkFile = os.path.join(link_dir, fileName)
            with open(file, "w") as f:
                f.write(bscAlarmContent)
            logger.info("generated {file}".format(file=file))
            os.symlink(file, linkFile)
            logger.info("{linkFile} link to {file}".format(linkFile=linkFile, file=file))

def houseKeeping():
    conf = ConfigParser.ConfigParser()
    conf.read(globVar.CONIF_FILE)
    link_dir = conf.get("external", "link_directory")
    logger.info("get symbolic link directory {dir}".format(dir=link_dir))

    try:
        cmd = "/bin/find {outputDir}/ -mtime +2 -type f -exec rm {char} \;".format(outputDir=globVar.OUTPUT_DIR, char="{}")
        status, ret = commands.getstatusoutput(cmd)
        if status==0:
            logger.info("run {cmd}".format(cmd=cmd))
        else:
            logger.info("run {cmd} failed".format(cmd=cmd))
        """
        cmd = "/bin/find {link_dir}/ -mtime 1 -type f -exec rm {char} \;".format(link_dir=link_dir, char="{}")
        status, ret = commands.getstatusoutput(cmd)
        if status==0:
            logger.info("run {cmd}".format(cmd=cmd))
        else:
            logger.info("run {cmd} failed".format(cmd=cmd))
        """
        # M
        fsize = os.path.getsize(globVar.LOG_FILE)/1024/2014
        if fsize >= 10:
            os.remove(globVar.LOG_FILE)
            logger.info("log file size {size}, remove {file}".format(size=fsize, file=globVar.LOG_FILE))
    except Exception, e:
        logger.error(e)

if __name__ == "__main__":
    genAlarm()
    houseKeeping()
