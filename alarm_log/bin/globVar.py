#!/usr/bin/env python
import os

# MAIN DIR
BASE_DIR = os.path.split(os.path.realpath(__file__))[0]

# Directory
CONF_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "conf"))
INPUT_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "input"))
LOG_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "log"))
OUTPUT_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "output"))
TEMP_DIR = os.path.abspath(os.path.join(BASE_DIR, "..", "temp"))

# Files
CONIF_FILE = os.path.abspath(os.path.join(CONF_DIR, "alarm_log.conf"))
LOG_FILE = os.path.abspath(os.path.join(LOG_DIR, "alarm_log.log"))
LOGGER_FILE = os.path.abspath(os.path.join(CONF_DIR, "logger.conf"))
TEMPLATE_FILE = os.path.abspath(os.path.join(CONF_DIR, "filter_template.txt"))

if __name__ == "__main__":
    print BASE_DIR
    print LOGGER_FILE